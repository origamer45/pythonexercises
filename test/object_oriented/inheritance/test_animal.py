import unittest

from source.object_oriented.inheritance.animal import Bird, Fish, Mammal, Human, BirdFish

class BirdTest(unittest.TestCase):
    def setUp(self):
        self._bird = Bird()

    def test_show_description(self):
        self.assertEqual("I am a bird", self._bird.show_description())

    def test_fly(self):
        self.assertEqual("Fly", self._bird.fly())

class FishTest(unittest.TestCase):
    def setUp(self):
        self._fish = Fish()

    def test_show_description(self):
        self.assertEqual("I am a fish", self._fish.show_description())

    def test_swim(self):
        self.assertEqual("Swim", self._fish.swim())

class MammalTest(unittest.TestCase):
    def setUp(self):
        self._mammal = Mammal()

    def test_show_description(self):
        self.assertEqual("I am a mammal", self._mammal.show_description())

class HumanTest(unittest.TestCase):
    def setUp(self):
        self._human = Human()

    def test_show_description(self):
        self.assertEqual("I am a mammal", self._human.show_description())


class BirdFishTest(unittest.TestCase):
    def setUp(self):
        self._bird_fish = BirdFish()

    def test_show_description(self):
        self.assertEqual("I am a bird and a fish", self._bird_fish.show_description())

    def test_fly(self):
        self.assertEqual("Fly", self._bird_fish.fly())

    def test_swim(self):
        self.assertEqual("Swim", self._bird_fish.swim())