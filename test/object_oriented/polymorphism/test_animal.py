import unittest

from source.object_oriented.polymorphism.animal import Mammal, Human

class MammalTest(unittest.TestCase):
    def setUp(self):
        self._mammal = Mammal()

    def test_show_description(self):
        self.assertEqual("I am a mammal", self._mammal.show_description())

class HumanTest(unittest.TestCase):
    def setUp(self):
        self._human = Human()

    def test_show_description(self):
        self.assertEqual("I am a mammal and a human", self._human.show_description())