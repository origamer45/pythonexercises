import unittest

from source.object_oriented.composition.person import Person

class PersonTest(unittest.TestCase):
    def test_present(self):
        person = Person("John", "Carver", 25, "male")

        self.assertEqual("Hello, my first name is John and my last_name is Carver. I am a male and I have 25 years old", person.present())

    def test_present_other_guy(self):
        person = Person("Caroline", "Full", 89, "female")

        self.assertEqual("Hello, my first name is Caroline and my last_name is Full. I am a female and I have 89 years old", person.present())