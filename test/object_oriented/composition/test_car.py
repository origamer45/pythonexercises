import unittest
import datetime

from source.object_oriented.composition.car import CarType, EnergyType, Motor, Wheel, Car

class TypeCarTest(unittest.TestCase):
    def test_check_exist_convertible(self):
        car_type = None

        try:
            car_type = CarType.CONVERTIBLE
        except AttributeError:
            self.assertTrue(False)

    def test_check_exist_suv(self):
        car_type = None

        try:
            car_type = CarType.SUV
        except AttributeError:
            self.assertTrue(False)

    def test_check_exist_pickup(self):
        car_type = None

        try:
            car_type = CarType.PICKUP
        except AttributeError:
            self.assertTrue(False)

    def test_check_exist_sedan(self):
        car_type = None

        try:
            car_type = CarType.SEDAN
        except AttributeError:
            self.assertTrue(False)

class EnergyTypeTest(unittest.TestCase):
    def test_check_exist_gasoline(self):
        energy_type = None

        try:
            energy_type = EnergyType.GASOLINE
        except AttributeError:
            self.assertTrue(False)

    def test_check_exist_chemical(self):
        energy_type = None

        try:
            energy_type = EnergyType.CHEMICAL
        except AttributeError:
            self.assertTrue(False)

    def test_check_exist_electricity(self):
        energy_type = None

        try:
            energy_type = EnergyType.ELECTRICITY
        except AttributeError:
            self.assertTrue(False)

class MotorTest(unittest.TestCase):
    def test_create_motor_electricity(self):
        consumption = 300.0
        energy_type = EnergyType.ELECTRICITY
        motor = Motor(consumption, energy_type)

        self.assertEqual(consumption, motor.consumption)
        self.assertEqual(energy_type, motor.energy_type)

    def test_create_motor_gasoline(self):
        consumption = 5.0
        energy_type = EnergyType.GASOLINE
        motor = Motor(consumption, energy_type)

        self.assertEqual(consumption, motor.consumption)
        self.assertEqual(energy_type, motor.energy_type)

class WheelTest(unittest.TestCase):
    def test_create_simple_wheel(self):
        width = 18
        weight = 400
        max_speed = 200
        date_production = datetime.date(2010, 10, 8)
        wheel = Wheel(width, weight, max_speed, date_production)

        self.assertEqual(width, wheel.width)
        self.assertEqual(weight, wheel.weight)
        self.assertEqual(max_speed, wheel.max_speed)
        self.assertEqual(date_production, wheel.date_production)

    def test_create_wheel_other_example(self):
        width = 14
        weight = 200
        max_speed = 500
        date_production = datetime.date(2018, 4, 8)
        wheel = Wheel(width, weight, max_speed, date_production)

        self.assertEqual(width, wheel.width)
        self.assertEqual(weight, wheel.weight)
        self.assertEqual(max_speed, wheel.max_speed)
        self.assertEqual(date_production, wheel.date_production)

class CarTest(unittest.TestCase):
    def test_create_car(self):
        name = "Imaginary car"
        car_type = CarType.SEDAN
        motor = Motor(4.2, EnergyType.ELECTRICITY)

        width = 14
        weight = 200
        max_speed = 500
        date_production = datetime.date(2018, 4, 8)
        wheel = Wheel(width, weight, max_speed, date_production)

        car = Car(name, car_type, motor, [wheel, wheel, wheel, wheel])

        self.assertEqual(name, car.name)
        self.assertEqual(car_type, car.car_type)
        self.assertEqual(motor.energy_type, EnergyType.ELECTRICITY)
        self.assertEqual(4, len(car.wheels))
        self.assertEqual(date_production, car.wheels[0].date_production)