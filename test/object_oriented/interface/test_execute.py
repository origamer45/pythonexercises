import unittest

from source.object_oriented.interface.execute import Programmer, SerialKiller, Citation

class ProgrammerTest(unittest.TestCase):
    def test_execute_programmer(self):
        programmer = Programmer()

        self.assertEqual("Programmer execute code", programmer.execute())

class SerailKillerTest(unittest.TestCase):
    def test_execute_programmer(self):
        serial_killer = SerialKiller()

        self.assertEqual("Serial killer execute people", serial_killer.execute())

class CitationTest(unittest.TestCase):
    def test_execute_programmer(self):
        citation = Citation()

        self.assertEqual("\"Execute code, not people\" from internet meme", citation.execute())