import unittest

from source.object_oriented.encapsulation.black_box import BlackBox

class BlackBoxTest(unittest.TestCase):
    def setUp(self):
        self._black_box = BlackBox(5)

    def test_get_number_hidden(self):
        self.assertEqual(5, self._black_box.number_hidden)

    def test_set_number_hidden(self):
        self.assertEqual(5, self._black_box.number_hidden)

        self._black_box.number_hidden = 86

        self.assertEqual(86, self._black_box.number_hidden)