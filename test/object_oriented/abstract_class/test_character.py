import unittest

from source.object_oriented.abstract_class.character import Villager, Soldier, Knight

class VillagerTest(unittest.TestCase):
    def setUp(self):
        self._villager = Villager()

    def test_get_name(self):
        self.assertEqual("Villager", self._villager.name)

    def test_move(self):
        self.assertEqual("Walk", self._villager.move())

    def test_attack(self):
        self.assertEqual("No attack", self._villager.attack())

class SoldierTest(unittest.TestCase):
    def setUp(self):
        self._soldier = Soldier()

    def test_get_name(self):
        self.assertEqual("Soldier", self._soldier.name)

    def test_move(self):
        self.assertEqual("Walk", self._soldier.move())

    def test_attack(self):
        self.assertEqual("Attack with gun", self._soldier.attack())

class KnightTest(unittest.TestCase):
    def setUp(self):
        self._knight = Knight()

    def test_get_name(self):
        self.assertEqual("Knight", self._knight.name)

    def test_move(self):
        self.assertEqual("Race", self._knight.move())

    def test_attack(self):
        self.assertEqual("Attack with sword", self._knight.attack())