#macros
PYTHON = python3

#rules
.PHONY: clean
clean:
	find . -name *.pyc -type f -delete
	find . -name __pycache__ -type d -delete

.PHONY: test
test:
	$(PYTHON) -m unittest