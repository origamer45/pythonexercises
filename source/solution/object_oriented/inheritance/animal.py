"""
Inheritance allow an inherited class (or subclass) to have all behaviors from parent class. With this, we can share different behaviors from parent class.
In Python, we can have multiple inheritances in contrary to Java.
For example, we want to implement a representation of different animals species:
- bird
- fish
- mammal
    - Human
- birdFish (combination of a bird and a fish)
"""

class Bird:
    def show_description(self):
        return "I am a bird"

    def fly(self):
        return "Fly"

class Fish:
    def show_description(self):
        return "I am a fish"

    def swim(self):
        return "Swim"

class Mammal:
    def show_description(self):
        return "I am a mammal"

class Human(Mammal):
    pass

class BirdFish(Bird, Fish):
    def show_description(self):
        return "I am a bird and a fish"