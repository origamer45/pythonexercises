"""
Encapsulation is a principle to hide implementation about a class like a black box. It purposes an interface to dialogue with this class.
Why is it useful to hide the implementation? It allows to avoid breaking some behaviors that we have definined in our classes. 
To access to attributes, we can use properties. These allow us to implement an interface between the user and our class to manipulate safety these attributes.

In Python, we don't have encapsulation. It is why there is a convention for encapsulation that if a attribut or a method start with underscore (for example, self._box), it means it is private attribut or method.
"""

class BlackBox:
    def __init__(self, number_to_hide):
        self._number_hidden = number_to_hide

    def _get_number_hidden(self):
        return self._number_hidden

    def _set_number_hidden(self, value):
        self._number_hidden = value

    number_hidden = property(_get_number_hidden, _set_number_hidden)