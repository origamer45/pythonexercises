"""
Interface allows to programmers to communicate with an object that implement this through different publics methods.
In Python, we don't have an interface technically beacause this langage is not strong typed but we can simulate this if we want to implement.
For example, we want to implement an interface (IExecute) for different classes (Programmer, SerialKiller and Citation)
"""

class IExecute:
    """An interface with one method execute. I use C# convention because it shows clearly that is an interface (I for interface)"""
    def execute(self):
        """Return action description"""
        pass

class Programmer(IExecute):
    def execute(self):
        return "Programmer execute code"

class SerialKiller(IExecute):
    def execute(self):
        return "Serial killer execute people"

class Citation(IExecute):
    def execute(self):
        return "\"Execute code, not people\" from internet meme"