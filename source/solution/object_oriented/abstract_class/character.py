"""
Abstract class is a generic class that we use to implement some methods or let implementations to subclasses. Abstract classes are not instantiated directly. We need need to use a subclasses.
For instance, we want to implement different characters (Villager, Soldier and Knight) based on AbstractCharacter class
"""

class AbstractCharacter:
    def __init__(self, name):
        self._name = name

    def _get_name(self):
        return self._name

    name = property(_get_name)

    def move(self):
        return "Walk"

    def attack(self):
        pass

class Villager(AbstractCharacter):
    def __init__(self):
        super().__init__("Villager")

    def attack(self):
        return "No attack"

class Soldier(AbstractCharacter):
    def __init__(self):
        super().__init__("Soldier")

    def attack(self):
        return "Attack with gun"

class Knight(AbstractCharacter):
    def __init__(self):
        super().__init__("Knight")

    def move(self):
        return "Race"

    def attack(self):
        return "Attack with sword"