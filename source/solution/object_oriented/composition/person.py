"""
Object composition refers to a combination of types or object in order to represent a new complex object. 
For example, we want to represent a person and we know it has different attributes:
- first name
- last name
- age
- gender (we will use a string not a class that we will create because we just want to keep this exercise as simple as possible)
"""

class Person:
    def __init__(self, first_name, last_name, age, gender):
        self.first_name = first_name
        self.last_name = last_name
        self.age = age
        self.gender = gender

    def present(self):
        format = "Hello, my first name is {0} and my last_name is {1}. I am a {3} and I have {2} years old".format(self.first_name, self.last_name, self.age, self.gender)

        return format