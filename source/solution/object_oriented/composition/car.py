"""
Let's take a second example for composition.
We want to represent a simple car:
- name
- type of car (convertible, SUV, pickup, sedan, ...). Use a type enumeration
- motor
    - consumption (liter / 100 kilometers)
    - type of energy (gasoline, chemical, electricity, ...). Use a type enumeration
- wheels
    - width of the wheel in inch
    - weight that the wheel can support in kilogram
    - max speed that the wheel can support in kilometer
    - date of the production
"""

from enum import Enum

class CarType(Enum):
    CONVERTIBLE = 1
    SUV = 2
    PICKUP = 3
    SEDAN = 4

class EnergyType(Enum):
    GASOLINE = 1
    CHEMICAL = 2
    ELECTRICITY = 3

class Motor:
    def __init__(self, consumption, energy_type):
        self.consumption = consumption
        self.energy_type = energy_type

class Wheel:
    def __init__(self, width, weight, max_speed, date_production):
        self.width = width
        self.weight = weight
        self.max_speed = max_speed
        self.date_production = date_production

class Car:
    def __init__(self, name, car_type, motor, wheels):
        self.name = name
        self.car_type = car_type
        self.motor = motor
        self.wheels = wheels