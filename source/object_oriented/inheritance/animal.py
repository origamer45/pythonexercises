"""
Inheritance allow an inherited class (or subclass) to have all behaviors from parent class. With this, we can share different behaviors from parent class.
In Python, we can have multiple inheritances in contrary to Java.
For example, we want to implement a representation of different animals species:
- bird
- fish
- mammal
    - Human
- birdFish (combination of a bird and a fish)
"""

