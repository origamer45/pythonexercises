"""
Abstract class is a generic class that we use to implement some methods or let implementations to subclasses. Abstract classes are not instantiated directly. We need need to use a subclasses.
For instance, we want to implement different characters (Villager, Soldier and Knight) based on AbstractCharacter class
"""

