"""
Polymorphism is linked with inheritance and it means take different forms. It allows to a subclass to have an other behavior than the parent class.
For example, we want to implement a representation of different animals species with specification behavior:
- mammal
    - Human
"""

class Mammal:
    def show_description(self):
        return "I am a mammal"

class Human(Mammal):
    def show_description(self):
        parent_string = super().show_description()
        format = "{} and a human".format(parent_string)

        return format