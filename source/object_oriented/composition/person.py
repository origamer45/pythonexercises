"""
Object composition refers to a combination of types or object in order to represent a new complex object. 
For example, we want to represent a person and we know it has different attributes:
- first name
- last name
- age
- gender (we will use a string not a class that we will create because we just want to keep this exercise as simple as possible)
"""

