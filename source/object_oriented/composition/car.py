"""
Let's take a second example for composition.
We want to represent a simple car:
- name
- type of car (convertible, SUV, pickup, sedan, ...). Use a type enumeration
- motor
    - consumption (liter / 100 kilometers)
    - type of energy (gasoline, chemical, electricity, ...). Use a type enumeration
- wheels
    - width of the wheel in inch
    - weight that the wheel can support in kilogram
    - max speed that the wheel can support in kilometer
    - date of the production
"""

from enum import Enum
