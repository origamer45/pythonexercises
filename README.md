# PythonExercises

## Description

Exercises in Python with solutions that allow you to practise specifics concepts. Some exercises may be not very useful in daily live but these show you how to apply with unit tests.

For executing tests, you can use the command make **"make test"** if you have make installed or use directly **"python3 -m unittest"** to execute all tests.

To avoid to have a lot of errors at the beginning, you can put in comment some unit tests you haven't done yet.

If you want to execute tests by file, you can specify the path. For example: **"python3 -m unittest test/object_oriented/polymorphism/test_animal.py"**.

## Order of exercises

### Object oriented

- Composition
- Encapsulation
- Inheritance
- Polymorphism
- Interface
- Abstract class